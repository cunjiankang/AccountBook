package com.accountbook.data;

/**
 * @author Airsaid
 * @github https://github.com/airsaid
 * @date 2017/4/20
 * @desc 帐薄封面类
 */
public class BookCover {
    public String cover;
    public boolean isSelect;
}
