package com.accountbook.mvp.count;

import com.accountbook.base.BasePresenter;
import com.accountbook.base.BaseView;
import com.accountbook.data.Account;
import com.accountbook.data.Error;
import com.accountbook.data.User;

import java.util.List;

/**
 * @author Airsaid
 * @github https://github.com/airsaid
 * @date 2017/5/12
 * @desc 统计 契约类
 */
public interface CountContract {

    interface View extends BaseView<Presenter>{
        void queryAccounts(String startDate, String endDate, int queryType, int type);
        void queryAccountsSuccess(List<Account> accounts);
        void queryAccountsFail(Error e);
        void setChartData();
        double getTotalMoney();
        void setQueryType(int type);
    }

    interface Presenter extends BasePresenter{
        void queryAccounts(User user, String startDate, String endDate, int queryType, int type);
    }

}
