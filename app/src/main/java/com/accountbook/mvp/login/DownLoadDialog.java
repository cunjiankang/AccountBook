package com.accountbook.mvp.login;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.Display;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.accountbook.R;

public class DownLoadDialog extends Dialog {
    private ProgressBar progressBar;

    public DownLoadDialog(@NonNull Context context) {
        super(context);
        setContentView(R.layout.layout_download);
        progressBar = findViewById(R.id.proggressbar);
    }

    public void updateProgress(int progress) {
        progressBar.setProgress(progress);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = getWindow();
        if (null != window) {
            window.setGravity(Gravity.CENTER);
            WindowManager manager = window.getWindowManager();
            Display d = manager.getDefaultDisplay();
            WindowManager.LayoutParams params = window.getAttributes();
            params.width = d.getWidth() * 5 / 6;
            params.height = d.getHeight() / 2;
            window.setAttributes(params);
        }
    }

}
