package com.accountbook.mvp.register;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;

import com.accountbook.R;
import com.accountbook.base.BaseActivity;
import com.accountbook.data.source.UserRepository;
import com.accountbook.util.ActivityUtils;
import com.accountbook.util.UiUtils;

/**
 * @author Airsaid
 * @Date 2017/2/21 22:01
 * @Blog http://blog.csdn.net/airsaid
 * @Desc 注册 Activity
 */
public class RegisterActivity extends BaseActivity{

    @Override
    public int getLayoutRes() {
        return R.layout.activity_base_toolbar;
    }

    @Override
    public void onCreateActivity(@Nullable Bundle savedInstanceState) {
        Toolbar toolbar = initToolbar(UiUtils.getString(R.string.title_register));
        toolbar.setBackgroundResource(R.color.colorPrimary);

        // set fragment
        RegisterFragment fragment =
                (RegisterFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);
        if (fragment == null) {
            // Create the fragment
            fragment = RegisterFragment.newInstance();
            ActivityUtils.addFragmentToActivity(
                    getSupportFragmentManager(), fragment, R.id.contentFrame);
        }

        // create the presenter
        new RegisterPresenter(new UserRepository(), fragment);
    }
}
