package com.accountbook.constants;

/**
 * Created by zhouyou on 2016/6/22.
 * Class desc:
 *
 * APP 常量类，用于存放APP中用到的常量。
 */
public class AppConstants {

    public static final String CONTENT_TYPE_FILE = "multipart/form-data";

    public static final String CONTENT_TYPE_JPG = "icon/jpg";

    public static final String CONTENT_TYPE_PNG = "icon/png";

    public static final String CONTENT_TYPE_TEXT = "text/plain";


    // SharedPreferences key Name
    public static String KEY_IS_FIRST_LOGIN         = "is_first_login";
    public static String KEY_BID                    = "bid";
    public static String KEY_LAST_UPDATE_TIME       = "lastUpdateTime";


    public static String EXTRA_ACCOUNT_TYPE     = "com.accountbook.ACCOUNT_TYPE";
    public static String EXTRA_POSITION         = "com.accountbook.POSITION";
    public static String EXTRA_DATA             = "com.accountbook.DATA";
    public static String EXTRA_TYPE             = "com.accountbook.TYPE";
    public static String EXTRA_IS_UPDATE_THEME  = "com.accountbook.IS_UPDATE_THEME";
    public static String EXTRA_TYPE_NAME        = "com.accountbook.TYPE_NAME";
    public static String EXTRA_START_DATE       = "com.accountbook.START_DATE";
    public static String EXTRA_END_DATE         = "com.accountbook.END_DATE";


}
